
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function index()
	{

		$this->load->model('about_model');
		$this->load->model('pages_model');
		$this->load->model('contact_model');

        $viewData = new stdClass();

       
        $viewData->about = $this->about_model->get_all(array(),"id ASC");
        $viewData->contents = $this->pages_model->get_all(array("page_url" => "home"),"rank ASC");
         $viewData->rows = $this->pages_model->get_all(array("isActive" => "1"),"rank ASC");

         $viewData->title       = 'Contact';

        // $viewData->home = $this->db->get('pages')->result();
        

		$this->load->view('contact', $viewData);
	}

	public function MsgSend(){

		$this->load->model('contact_model');

		$data = array( "name" => $this->input->post('name') ,

						"surname" => $this->input->post('surname') ,

						"email" => $this->input->post('email') ,

						"message" => $this->input->post('message') ,

						
		 );
		
		$this->db->set('date_message', 'NOW()', FALSE);

		$insert =  $this->contact_model->add($data);
		

		if ($insert) {
			redirect(base_url('contact'));
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
