<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <form role="form" method="post" action="<?php echo base_url("users/add");?>">
                    <div class="box-body col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1"> Adı</label>
                            <input type="text" class="form-control" name="name" placeholder="Kategori adını giriniz..">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">SoyAdı</label>
                            <input type="text" class="form-control" name="surname" placeholder="Kategori adını giriniz..">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" class="form-control" name="email" placeholder="Kategori adını giriniz..">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Şifre</label>
                            <input type="text" class="form-control" name="password" placeholder="Kategori adını giriniz..">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Kullanıc Adı</label>
                            <input type="text" class="form-control" name="username" placeholder="Kategori adını giriniz..">
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="clearfix"></div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Ekle</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
</section>
<!-- /.content -->