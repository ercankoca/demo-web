<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model("pages_model");
	}

	public function index()
	{

		$viewData = new stdClass();
		$viewData->rows = $this->pages_model->get_all(array(),"id ASC");

		$this->load->view('pages', $viewData);
	}

	

	public function newPage()
	{
		$this->load->view("new_page");
	}

	public function editPage($id) 
	{

		$viewData = new stdClass();

		$viewData->row = $this->pages_model->get( array("id" =>  $id ));

		$this->load->view("edit_pages", $viewData);
	}


	public function add()

		{
			$data = array('page' => $this->input->post('page'));
			$insert = $this->pages_model->add($data);

			if ($insert) {
				redirect(base_url('pages'));
			}

			else {
				redirect(base_url('pages/newPage'));
			}
		}

	public function edit($id)
	{
		$data =  array(	'page' => $this->input->post("page")
								

		 );

		$update = $this->pages_model->update(

			 array('id' => $id ), $data);

		if ($update) {
			redirect(base_url('pages'));
		}

		else{
			redirect(base_url('pages/editPage/$id'));
		}
	}

	public  function isActiveSetter()
	{
		$id   =  $this->input->post("id");
		$isActive = ($this->input->post("isActive")=="true") ? 1 : 0;

		$update = $this->pages_model->update(

			array('id' => $id),
			array('isActive'  => $isActive)

		);
	}

	public function delete($id)
	{
		$delete = $this->pages_model->delete(array("id" => $id));

		redirect(base_url("pages"));
	}

	


	}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */